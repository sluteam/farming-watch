import React from 'react'
import AjaxExample from './../components/AjaxExample'

const Home = () => {
  return (
    <div>
      Homepage
      <AjaxExample />
    </div>
  )
}

export default Home
