import React, { useState, useEffect } from "react";

const url = "https://api.github.com/users";

const AjaxExample = () => {
  const [users, setUsers] = useState([]);

  const getUsers = async () => {
    const response = await fetch(url);
    const users = await response.json();
    setUsers(users);
    // console.log(users);
  };

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <>
      <a href="https://api.github.com/users" target="_blank" rel="noreferrer" className="block text-blue-400 underline hover:no-underline hover:text-blue-700">
        https://api.github.com/users
      </a>
      <h3 className="text-center my-12 font-bold">Github Users</h3>
      <ul className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 mx-12">
        {users.map((user) => {
          const { id, login, avatar_url, html_url } = user;
          return (
            <li key={id} className=" flex bg-blue-50 rounded-lg px-4 py-8">
              <img
                src={avatar_url}
                alt={login}
                className="w-14 h-14 rounded-full mr-4"
              />
              <div>
                <h4 className="font-bold capitalize mb-1">{login}</h4>
                <a
                  href={html_url}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="capitalize hover:underline text-blue-500"
                >
                  profile
                </a>
              </div>
            </li>
          );
        })}
      </ul>
    </>
  );
};

export default AjaxExample;
